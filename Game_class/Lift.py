import pygame
import threading
from time import *
from random import *
class Lift(object):
    """Объект игрока"""

    lifts = []
    num = 0
    def __init__(self,x,y,size,h):
        #Создание объекта кубика
        self.playerOnLift = False
        self.size = size
        self.images = pygame.Surface((size[0],size[1]))
        #Закраска кубика в другой цвет
        self.images.fill((255,255,0))
        Lift.lifts.append(self.images)
        #Начальные координаты объекта относительно окна мира
        self.x = x
        self.y = y
        #Верхний и нижний этаж
        self.up = y
        self.down = y + h
        #Скорость лифта
        self.vy = 1
        #Переменная опускания
        self.RUN_DOWN = True
        self.timer = pygame.time.Clock()
    def searchKubik(self):
        if Lift.sprite.x >= self.x - 40 and Lift.sprite.x < self.x:
            if Lift.sprite.y < self.y and Lift.sprite.y >= self.y - 40:
                self.playerOnLift = True
                #Lift.sprite.vy = 0
            else:
                self.playerOnLift = False
    def update(self,lifts):
        """Метод передвигающий блок"""
        while True:
            self.timer.tick(60)
            for i in lifts:
                i.searchKubik()
                if i.RUN_DOWN:
                    if i.playerOnLift:
                        Lift.sprite.winy -= i.vy
                        Lift.sprite.y = -Lift.sprite.winy + 400
                    i.y += i.vy

                else:
                    if i.playerOnLift:
                        Lift.sprite.winy += i.vy
                        Lift.sprite.y = -Lift.sprite.winy + 400
                    i.y -= i.vy
                if i.y >= i.down:
                    i.RUN_DOWN = False
                if i.y <= i.up:
                    i.RUN_DOWN = True
            if Lift.sprite.die == True:
                break
        print('Поток лифта выключен.')
    def start(self,lifts,sprite):
        Lift.sprite = sprite
        self.t = threading.Thread(target=Lift.update, args=(self,lifts))
        #self.t.daemon = True
        self.t.start()
    def readr(self,window,winx,winy,lifts):
        """Метод ставящий объекты класса в окно мира"""
        for i in lifts:
            #Ставит объект на главное окно
            window.blit(i.images, (i.x + winx,i.y + winy))




class Lazer():
    def __init__(self,x,y,screen,window,napravlenie):
        self.screen = screen
        self.window = window
        self.x = x
        self.y = y

        self.image = pygame.Surface((20,20))
        self.imagep = pygame.Surface((5,5))
        self.luch = pygame.Surface((500,5))
        self.luch.fill((255,0,0))
        self.image.fill((255,255,255))
        self.imagep.fill((255,255,255))
        self.napravlenie = napravlenie

        self.shootB = False

        self.timestart = 0
        self.timeend = 0
        self.timeshoot = 1
    def blits(self):
        if self.napravlenie:
            self.xx = self.x + 20
            self.yy = self.y + 7.5
            self.screen.blit(self.image,(self.x,self.y))
            self.screen.blit(self.imagep,(self.x + 20,self.y + 7.5))
        else:
            self.xx = self.x - 5
            self.yy = self.y + 7.5
            self.screen.blit(self.image,(self.x,self.y))
            self.screen.blit(self.imagep,(self.x - 5,self.y + 7.5))

    def shoot(self):
        if time() - self.timeend > self.timeshoot:
            self.shootB = True
            self.timestart = time()
            self.timeshoot = randint(1,2)
    def stopShoot(self):
        if time() - self.timestart > self.timeshoot:
            self.shootB = False
            self.timeend = time()
    def kill_sprite(self,sx,sy,lazer):
        if lazer.shootB:
            if lazer.napravlenie:
                lx = lazer.xx + 5
            else:
                lx = lazer.xx - 500
            ly = lazer.yy
            if ly >= sy and sy + 30 >= ly + 10:
                if lx + 500 >= sx and sx >= lx:
                    Lazer.sprite.die = True
    def update(self,lazers):
        while True:
            for i in lazers:
                shoot = randint(0,100)
                if shoot > 50:
                    if i.shootB == False:
                        i.shoot()
                    i.kill_sprite(Lazer.sprite.x,Lazer.sprite.y,i)
                else:
                    if i.shootB == True:
                        i.stopShoot()
                    i.kill_sprite(Lazer.sprite.x,Lazer.sprite.y,i)
            if Lazer.sprite.die:
                break

    def start(self,lazers,sprite):
        Lazer.sprite = sprite
        self.t = threading.Thread(target=Lazer.update, args=(self,lazers))
        self.t.start()

    def readr(self,lazers,window,winx,winy):
        for i in lazers:
            if i.shootB:
                if i.napravlenie:
                    window.blit(i.luch,(i.xx + 5 + winx,i.yy + winy))
                else:
                    window.blit(i.luch,(i.xx - 500 + winx,i.yy + winy))






# def clock(interval):
#     while True:
#         print("The time is %s" % time.ctime())
#         time.sleep(interval)
# t = threading.Thread(target=clock, args=(15,))
# t.daemon = True
# t.start()