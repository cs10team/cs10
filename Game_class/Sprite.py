import pygame
from time import *
"""Объект игрока принимает на вход: Массив картинок(для анимации), начальую позицию,
    массив блоков координатно, позицию главного окна.
    Принцип движения - изменение координат  окна мира ,
    оставляя объект ровно по середине главного окна."""
class Player(object):
    """Объект игрока"""
    def __init__(self,SIZE = (),pos = (),world = [], poswindow = (),G = 0.2):
        #Блоки пола , стен и потолка в координатах
        self.world = world
        self.lx = pos[0] * 2
        self.ly = pos[1] + 200
        #Начальная координата пола. Ниже которого объект не может опустится
        #Начальная координата потолка и стен - левой и правой
        self.pol = self.ly - 600
        self.polp = 600
        self.stenal = 600
        self.stenar = self.lx -600
        #Создание объекта кубика
        #self.images = pygame.Surface((40,40))
        self.images = pygame.image.load('Game_class/picture/sprits/spiderman.png')
        self.imagedie = pygame.image.load('Game_class/picture/sprits/spidermandie.png')
        #Закраска кубика в другой цвет
        #self.images.fill((255,255,255))
        self.die = False
        self.G = G
        #Начальная координата окна мира
        self.winy = poswindow[1]
        self.winx = poswindow[0]

        #Начальные координаты объекта относительно окна мира
        self.x = -self.winx + 600
        self.y = -self.winy + 400

        #Начальная скорость объкта по оси y
        self.vy = 0
        #Начальная скорость объкта по оси х
        self.vx = 10
        #Скорость объекта по оси y при начале прыжка
        self.mvy = -15

        #Переменная отвечающая за состояния объекта
        #Нахождения его в прыжке/полете
        self.jum = True

    def run(self,flag):
        """Метод бега.При вызове изменяет координату по оси х"""
        #Условие чтобы игрок не вышел за поле окна мира
        if self.die == False:
            if self.x > self.stenal and self.x < self.stenar - 40:
                if flag == 1:
                    self.winx -= self.vx
                else:
                    self.winx += self.vx
            if self.x <= self.stenal and flag == 1:
                self.winx -= self.vx
            if self.x >= self.stenar - 40 and flag == 0:
                self.winx += self.vx
            self.x = -self.winx + 600
    def jump(self):
        """Метод прыжка.При вызове изменяет координату объекта по оси y"""
        #Условие , чтобы нельзя было повторять прыжок в полете
        if self.die == False:
            if self.jum == True:
                self.jum = False
                self.vy = self.mvy
    def searchsten(self):
        """Метод поиска стены"""
        lsten = []
        rsten = []
        for i in self.world:
            if self.x > i[0]:
                if self.y > i[2] and self.y < i[2] + i[3]:
                    lsten.append(i[1])
            if self.x < i[1]:
                if self.y > i[2] and self.y < i[2] + i[3]:
                    rsten.append(i[0])
        if len(lsten) > 0:
            self.stenal = max(lsten)
        else:
            self.stenal = 600
        if len(rsten) > 0:
            self.stenar = min(rsten)
        else:
            self.stenar = self.lx - 600
    def searchpol(self,lifts):
        """Метод ищущий пол под объектом"""
        pols = []
        potols = []
        world = self.world + lifts
        for i in world:
            if self.x > i[0] - 40 and self.x < i[1]:
                if i[2] - 20  >= round(-self.winy + 400):
                    pols.append(i[2])
                if i[2] + i[3] <= round(-self.winy + 400):
                    if len(i) != 5:
                        potols.append(i[2] + i[3])
        #print(pols)
        if len(pols) > 0:
            self.pol = min(pols)
        else:
            self.pol = self.ly - 600
        if len(potols) > 0:
            self.polp = max(potols)
        else:
            self.polp = 600
    def down(self,screen):
        """Метод прохода вниз через блок(не используется)"""
        if self.jum and self.y < self.ly - 100:
            self.y += 10
            self.winy -= 10
    def update(self,lifts,window):
        """Метод ставящий объект в окно мира"""
        #Ищет пол и стены
        if self.die == False:
            self.searchpol(lifts)
            self.searchsten()
            if round(-self.winy + 400) < self.polp + 11:
                self.vy = 0
                self.y = self.polp + 11
                self.winy = 400 - self.y
            #Проверяет находится ли объект над полом или взлетает
            #Если падает проверяется условие (round(self.y) <= self.pol - 40
            #Если взлетает , то подходит условие self.vy < 0
            if (round(-self.winy + 400) < self.pol - 41  or self.vy < 0):
                #Изменяем переменную полета , чтобы не повторить прыжок в полете
                self.jum = False
                self.winy -= self.vy
                self.y = -self.winy + 400
                #Ограничение скорости падения скоростью взлета
                if self.vy < -self.mvy:
                    #print(self.G)
                    self.vy += self.G
            else:
                #Выравнивает объект на полу
                # if self.pol - 40 < -self.winy + 400:
                #     self.winy += 2
                #     self.vy = 0
                #Тоже типа выравнивает
                if self.vy == 0:
                    self.y = self.pol - 40
                    self.winy = 400 - self.y
                self.vy = 0
                #Позволяет снова прыгнуть
                self.jum = True
            window.blit(self.images, (600,400))
        else:
            if round(-self.winy + 400) < self.polp + 11:
                self.vy = 0
                self.y = self.polp + 11
                self.winy = 400 - self.y
            #Проверяет находится ли объект над полом или взлетает
            #Если падает проверяется условие (round(self.y) <= self.pol - 40
            #Если взлетает , то подходит условие self.vy < 0
            if (round(-self.winy + 400) < self.pol - 41  or self.vy < 0):
                #Изменяем переменную полета , чтобы не повторить прыжок в полете
                self.jum = False
                self.winy -= self.vy
                self.y = -self.winy + 400
                #Ограничение скорости падения скоростью взлета
                if self.vy < -self.mvy:
                    #print(self.G)
                    self.vy += self.G
            else:
                #Выравнивает объект на полу
                # if self.pol - 40 < -self.winy + 400:
                #     self.winy += 2
                #     self.vy = 0
                #Тоже типа выравнивает
                if self.vy == 0:
                    self.y = self.pol - 40
                    self.winy = 400 - self.y
                self.vy = 0
                #Позволяет снова прыгнуть
                self.jum = True
            window.blit(self.imagedie, (600,400))



