import pygame
class Npc(object):
    """Объект игрока"""
    def __init__(self,images = [],pos = (),world = [], poswindow = (),G = 1):
        #Блоки пола , стен и потолка в координатах
        self.world = world
        self.lx = pos[0] * 2
        self.ly = pos[1] + 200
        #Начальная координата пола. Ниже которого объект не может опустится
        #Начальная координата потолка и стен - левой и правой
        self.pol = self.ly - 600
        self.polp = 600
        self.stenal = 600
        self.stenar = self.lx -600
        #Создание объекта кубика
        self.images = pygame.Surface((40,40))
        #Закраска кубика в другой цвет
        self.images.fill((255,0,0))

        #Получаем по данным формулам размер мира основного

        #Начальная координата окна мира
        self.winy = poswindow[1]
        self.winx = poswindow[0]

        #Начальные координаты объекта относительно окна мира
        self.x = 900
        self.y = 3950

        #Начальная скорость объкта по оси y
        self.vy = 0

        #Скорость объекта по оси y при начале прыжка
        self.mvy = -10
        self.G = G
        #Переменная отвечающая за состояния объекта
        #Нахождения его в прыжке/полете
        self.jum = True

    def run(self , vx, screen):
        """Метод бега.При вызове изменяет координату по оси х"""
        #Условие чтобы игрок не вышел за поле окна мира
        if (self.x <= self.stenar - 45 and vx > 0)  or (self.x >= self.stenal + 5 and vx < 0) or (self.x >= self.stenal + 5 and self.x <= self.stenar - 45):
            self.x -= vx
    def jump(self):
        """Метод прыжка.При вызове изменяет координату объекта по оси y"""
        #Условие , чтобы нельзя было повторять прыжок в полете
        if self.jum == True:
            self.jum = False
            self.vy = self.mvy
    def searchsten(self):
        """Метод поиска стены"""
        lsten = []
        rsten = []
        for i in self.world:
            if self.x > i[0]:
                if self.y > i[2] and self.y < i[2] + i[3]:
                    lsten.append(i[1])
            if self.x < i[1]:
                if self.y > i[2] and self.y < i[2] + i[3]:
                    rsten.append(i[0])
        if len(lsten) > 0:
            self.stenal = max(lsten)
        else:
            self.stenal = 600
        if len(rsten) > 0:
            self.stenar = min(rsten)
        else:
            self.stenar = self.lx - 600
    def searchpol(self):
        """Метод ищущий пол под объектом"""
        pols = []
        potols = []
        for i in self.world:
            if self.x > i[0] - 40 and self.x < i[1]:
                if i[2] - 30  >= self.y:
                    pols.append(i[2])
                if i[2] + i[3] <= self.y:
                    if len(i) != 5:
                        potols.append(i[2] + i[3])
        #print(pols)
        if len(pols) > 0:
            self.pol = min(pols)
        else:
            self.pol = self.ly - 600
        if len(potols) > 0:
            self.polp = max(potols)
        else:
            self.polp = 600
    def down(self,screen):
        """Метод прохода вниз через блок(не используется"""
        if self.jum and self.y < self.ly - 100:
            self.y += 10
            self.winy -= 10
    def readr(self, screen,window,winx,winy):
        """Метод ставящий объект в окно мира"""
        #Ищет пол и стены
        self.searchpol()
        self.searchsten()

        if round(self.y) < self.polp + 11:
            self.vy = 0
            self.y = self.polp + 11
        #Проверяет находится ли объект над полом или взлетает
        #Если падает проверяется условие (round(self.y) <= self.pol - 40
        #Если взлетает , то подходит условие self.vy < 0
        if (round(self.y) < self.pol - 40  or self.vy < 0):
            #Изменяем переменную полета , чтобы не повторить прыжок в полете
            self.jum = False
            self.y += self.vy
            #Ограничение скорости падения скоростью взлета
            if self.vy < -self.mvy:
                self.vy += self.G
        else:
            #Выравнивает объект на полу
            # if self.pol - 40 < -self.winy + 400:
            #     self.winy += 2
            #     self.vy = 0
            #Тоже типа выравнивает
            if self.vy == 0:
                self.y = self.pol - 40
            self.vy = 0
            #Позволяет сно ва прыгнуть
            self.jum = True
        #print(self.x,self.y)
        #Ставит объект на главное окно
        window.blit(self.images, (self.x + winx,self.y + winy))