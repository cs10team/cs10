import pygame
from Game_class.Lift import *
class Dekor(object):
    """Объект декораций"""
    def __init__(self,screen,lx,ly,world,lifts,lazers,sprite,window):
        self.lx = lx
        self.ly = ly
        self.pol = ly - 600
        self.world = world
        self.lifts = lifts
        self.lazers = lazers

        self.screen = screen
        self.sprite = sprite
        self.window = window


        self.adres_decor = 'Game_class/picture/decor/'
        self.adres_fon = 'Game_class/picture/fons/'
        self.adres_platform = 'Game_class/picture/platforms/'
        self.adres_stens = 'Game_class/picture/stens/'
        self.fonarn = 'fonar2.png'
    def fonar(self,x,y):
        file = self.adres_decor + self.fonarn
        temp = pygame.image.load(file)
        self.world.append([x+10,x+40,y + 20,5,0])
        self.screen.blit(temp,(x,y))
    def box(self,x,y,ntype):
        types = []
        types.append(self.adres_decor + 'box.png')
        types.append(self.adres_decor + 'boxs.png')

        temp = pygame.image.load(types[ntype])
        if ntype == 0:
            self.world.append([x,x+100,y,100,100])
            self.screen.blit(temp,(x,y))
        elif ntype == 1:
            self.world.append([x,x+50,y,100,50])
            self.screen.blit(temp,(x,y))
    def lift(self,x,y,size,h):
        lift = Lift(x,y,size,h)
        self.lifts.append(lift)
    def lazer(self,x,y,screen,window,napravlenie):
        lazer = Lazer(x,y,screen,window,napravlenie)
        lazer.blits()
        self.lazers.append(lazer)
    def fon(self,ntype):
        types = []
        types.append(self.adres_fon + 'kosmos.jpg')
        types.append(self.adres_fon + 'metal.jpg')
        fon = pygame.image.load(types[ntype])
        self.screen.blit(fon,(0,0))
        h = self.ly - 650
        zem = pygame.Surface((self.lx,650))
        zem.fill((0,20,0))
        self.screen.blit(zem,(0,h))
    def polp(self,ntype):
        x1 = 0
        ly = self.ly - 600
        for i in range(round(self.lx/100)):
            self.platform(x1,ly-50,ntype)
            x1 += 100
    def platform(self,x,y,ntype):
        types = []
        types.append(self.adres_platform + 'asvalt.jpg')
        types.append(self.adres_platform + 'metal.jpg')

        platform = pygame.image.load(types[ntype])
        self.world.append([x,x+100,y,10,0])
        self.screen.blit(platform,(x,y))
    def stena(self,x,y,ntype):
        types = []
        types.append(self.adres_stens + 'metal.jpg')
        types.append(self.adres_stens + 'asvalt.jpg')
        platform = pygame.image.load(types[ntype])
        self.world.append([x,x+10,y,100])
        self.screen.blit(platform,(x,y))
    def grid(self):
        level = Level
        level.level_2(self)

class Level(Dekor):

    def level_1(self):
        self.fon(0)
        self.polp(0)
        #self.fonar(600,ly - 350)
        self.fonar(1300,self.pol - 350)
        self.box(1100,self.pol - 100,1)
        self.box(1000,self.pol - 150,0)
        self.box(900,self.pol - 150,0)
        self.box(950,self.pol - 250,0)
        self.lift(800,self.pol - 610,(100,10),500)

        #self.lift(800,self.pol - 100,(100,10),100)
        n = 5
        t = 1500
        k = 10
        for i in range(n):
            l = t
            for j in range(40-i):
                self.platform(l,self.pol - 50 - k,0)
                l += 50
            self.platform(t,self.pol - 50 - k,0)
            t += 15
            k += 10

        n = 2
        t = 1700
        k = 360
        for i in range(n):
            l = t
            for j in range(40-i):
                self.platform(l,self.pol - 50 - k,0)
                l += 50
            self.platform(t,self.pol - 50 - k,0)
            #t += 50
            k += 10
        #Lift.start(self)
    def level_2(self):
        self.fon(0)
        self.polp(0)
        self.lift(800,self.pol - 260,(100,10),200)
        self.lift(900,self.pol - 460,(100,10),300)
        self.lift(1000,self.pol - 660,(100,10),400)
        self.lazer(700,self.pol - 260,self.screen,self.window,True)
        self.lazer(1200,self.pol - 460,self.screen,self.window,False)
        self.platform(800,self.pol - 660,0)
        self.platform(900,self.pol - 660,0)
        self.platform(1000,self.pol - 660,0)
        self.platform(1100,self.pol - 660,0)
        self.platform(1200,self.pol - 660,0)
