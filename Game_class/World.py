from Game_class.Dekor import *
from Game_class.Sprite import *
from Game_class.Npc import *
from Game_class.Lift import *
from time import *

class World(object):
    number = 0
    def __init__(self,window,screen,posscreen,SIZE,lx,ly):
        self.lx = lx
        self.ly = ly
        self.okna = []
        self.num = World.number
        World.number += 1

        self.screen = screen
        self.window = window
        self.GAME_FLAG = True
        self.posscreen = posscreen
        self.SIZE = SIZE


        #Гравитационная постоянная
        self.G = 0.8

        #Переменные движения
        self.LEFT_PRESSED = False
        self.RIGHT_PRESSED = False
        self.UP_PRESSED = False

        self.TIME = 0
        self.world = []
        self.lifts = []
        self.lazers = []
        #Создание мира блоков

        self.images = []
        self.sprite = Player(self.SIZE,(self.lx / 2,self.ly - 200),self.world, self.posscreen,self.G)
        #self.npc = Npc(self.SIZE,(self.lx / 2,self.ly - 200),self.world, self.posscreen,self.G)
        self.decor()

        self.timer = pygame.time.Clock()


        #pygame.key.set_repeat(1, 1)

    def decor(self):
        decor = Dekor(self.screen,self.lx,self.ly,self.world,self.lifts,self.lazers,self.sprite,self.window)
        decor.grid()

    def start(self):
        LEFT_PRESSED = False
        RIGHT_PRESSED = False
        UP_PRESSED = False
        self.GAME_FLAG = True
        # rasprig = 1
        self.command = ''
        print('Игровое окно')
        Lift.start(self,self.lifts,self.sprite)
        Lazer.start(self,self.lazers,self.sprite)
        while self.GAME_FLAG:

            self.timer.tick(60)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.GAME_FLAG = False
                    self.command = 'close'
                    break
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_RIGHT:
                        RIGHT_PRESSED = True
                    if e.key == pygame.K_LEFT:
                        LEFT_PRESSED = True
                    if e.key == pygame.K_SPACE:
                        print(self.sprite.x,self.sprite.y)
                    if e.key == pygame.K_UP:
                        UP_PRESSED = True
                    if e.key == pygame.K_q:
                        self.GAME_FLAG = False
                        self.command = 'lvlup'
                        break
                    if e.key == pygame.K_a:
                        if self.num != 0:
                            self.GAME_FLAG = False
                            self.command = 'lvldown'
                            break
                    if e.key == pygame.K_ESCAPE:
                        self.GAME_FLAG = False
                        self.command = 'menu'
                        break
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_RIGHT:
                        RIGHT_PRESSED = False
                    if e.key == pygame.K_LEFT:
                        LEFT_PRESSED = False
                    if e.key == pygame.K_UP:
                        UP_PRESSED = False
                        rasprig = 1
            if self.sprite.die == True:
                self.GAME_FLAG = False
                self.command = 'restart'
            if UP_PRESSED:
                self.sprite.jump()
                print(self.sprite.G)
            #self.npc.jump()
            if RIGHT_PRESSED:
                self.sprite.run(1)
            if LEFT_PRESSED:
                self.sprite.run(0)



            
            self.window.blit(self.screen,(self.sprite.winx,self.sprite.winy))
            #self.npc.readr(self.screen,self.window,self.sprite.winx,self.sprite.winy)
            Lift.readr(self,self.window,self.sprite.winx,self.sprite.winy,self.lifts)
            Lazer.readr(self,self.lazers,self.window,self.sprite.winx,self.sprite.winy)
            self.sprite.update(lifts_convert(self.lifts),self.window)

            pygame.display.update()
        print('Игровое окно закрыто')
        sleep(0.2)
        print(self.command)
        return self.command

def lifts_convert(lifts):
    worldlift = []
    for i in lifts:
        worldlift.append([i.x,i.x + i.size[0],i.y,i.size[1],0])
    return  worldlift