import pygame
from time import *
class Menu(object):
    def __init__(self,window,SIZE):
        self.window = window

        self.GAME_FLAG = True

        self.SIZE = SIZE
        self.screen = pygame.Surface(SIZE)
        self.screen.fill((0,0,135))
    def start(self):
        self.command = ''
        self.GAME_FLAG = True
        print('Меню')
        while self.GAME_FLAG:
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.GAME_FLAG = False
                    self.command = 'close'
                    break
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        self.GAME_FLAG = False
                        self.command = 'resume'
            self.window.blit(self.screen,(0,0))
            pygame.display.flip()
        print('Меню закрыто')
        sleep(0.2)
        return self.command
