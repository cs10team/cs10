import random

from Class.Sprite import *


def main():
    #lx,ly - размер экрана
    lx = 1200
    ly = 20000
    #Главный параметр игрового цикла
    GAME_FLAG = True
    SIZE = (1200,600)
    posscreen = (0,SIZE[1] - ly )
    color = (0,0,0)
    #Скорость и гравитационная постоянная
    V = 600
    G = 0.4
    worldd = [[0,100,ly - 350],[lx-100,lx,ly - 350],[lx/2 -50,lx/2 +50,ly - 400],[lx/2 -50,lx/2 + 50,ly - 800],[0,0,ly-500],[0,0,ly - 150],[0,lx,ly-50]]
    world = []
    t = 0
    for i in range(30):
        for j in range(len(worldd)-1):
            A = [ ]
            for i in worldd[j]:
                A.append(i)
            l = random.randint(50,100)
            xb = random.randint(0,lx - 100)
            A[2] -= t
            A[0] = xb
            A[1] = xb + l
            world.append(A)
        t += 600
    world.append(worldd[6])
    world.append([200,400,ly - 100])
    worldname = []
    for i in range(len(world)):
        worldname.append(i)

    window = pygame.display.set_mode(SIZE)
    pygame.display.set_caption('СS10')

    screen = pygame.Surface((lx,ly))
    screen.fill((0,0,0))
    window.blit(screen,posscreen)
    #screen2 = pygame.Surface((lx,ly))

    print(world)
    for i in range(len(world)):
        l = world[i][1]-world[i][0]
        worldname[i] = pygame.Surface((l,10))
        #print(worldd[i][1]-worldd[i][0])
        worldname[i].fill((220,220,220))
        screen.blit(worldname[i],(world[i][0],world[i][2]))
    window.blit(screen,posscreen)


    global screen,window

    images = []
    # imag = 'hero'
    #
    # for i in range(7):
    #     imagname = imag + str(i) + '.png'
    #     A = pygame.image.load(imagname)
    #     images.append(A)


    sprite = Player(images,(300,ly-200),world, posscreen)

    pygame.key.set_repeat(1, 1)

    LEFT_PRESSED = False
    RIGHT_PRESSED = False
    UP_PRESSED = False
    TIME = 0
    window.blit(screen,posscreen)
    kraska = 0
    while GAME_FLAG:
        DELTA = time() - TIME
        TIME = time()
        for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    GAME_FLAG = False
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_RIGHT:
                        RIGHT_PRESSED = True
                    if e.key == pygame.K_LEFT:
                        LEFT_PRESSED = True
                    if e.key == pygame.K_UP:
                        UP_PRESSED = True
                    if e.key == pygame.K_DOWN:
                        sprite.down()
                    if e.key == pygame.K_1:
                        sprite.flags()
                    if e.key == pygame.K_2:
                        sprite.x = sprite.flagx
                        sprite.y = sprite.flagy
                        sprite.winy = 400 - sprite.y
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_RIGHT:
                        RIGHT_PRESSED = False
                    if e.key == pygame.K_LEFT:
                        LEFT_PRESSED = False
                    if e.key == pygame.K_UP:
                        UP_PRESSED = False

        if UP_PRESSED:
            sprite.jump(DELTA)
        if RIGHT_PRESSED:
            vx = V * DELTA
            sprite.run(vx)

        if LEFT_PRESSED:
            vx = (-1) * V * DELTA
            sprite.run(vx)
        # if kraska % 100 == 0:
        #     screen.fill((0, 0, 0))
        # kraska += 1
        screen.fill((0,0,0))

        sprite.readr(screen,window,G)
        #screen.fill((0, 0, 0))
        for i in range(len(world)):
            #print(worldd[i][1]-worldd[i][0])
            if -sprite.winy - 10 < world[i][2] and -sprite.winy + SIZE[1] + 10 > world[i][2]:
                #worldname[i].fill((random.randint(0,254),random.randint(0,254),random.randint(0,254)))
                screen.blit(worldname[i],(world[i][0],world[i][2]))
        window.blit(screen,(sprite.winx,sprite.winy))


        sprite.readr(screen,window,G)
        pygame.display.flip()
main()


