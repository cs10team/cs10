import pygame
import time
class Main_screen:
    #Класс главыных окон
    def __init__(self,size = (),pos = (),color = (255,255,255)):
        self.color = color
        self.pos = pos
        self.screen = pygame.Surface(size)
        self.screen.fill(color)

    def reader(self):
        window.blit(self.screen,self.pos)
        self.screen.fill(self.color)

    def replace(self,pos = ()):
        window.blit(self.screen,pos)

def change_screens(tscreen,nscreen):
    posn = nscreen.pos
    post = tscreen.pos
    tscreen.replace(posn)
    nscreen.replace(post)
    nscreen.pos = post
    tscreen.pos = posn

if __name__ == '__main__' :
    hx = 400
    hy = 400
    SIZE = (hx,hy)
    GAME_FLAG = True
    pos1 = (0,0)
    pos2 = (0,hy)

    window = pygame.display.set_mode(SIZE)
    #global window
    pygame.display.set_caption('test2')


    game_screen = Main_screen(SIZE,pos1,(255,0,0))
    menu_screen = Main_screen(SIZE,pos2,(0,255,0))
    game_screen.reader()
    menu_screen.reader()
    pygame.display.flip()
    time.sleep(2)
    while GAME_FLAG:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                GAME_FLAG = False
        change_screens(menu_screen,game_screen)
        time.sleep(0.1)
        pygame.display.flip()