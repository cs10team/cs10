from tkinter import *
from tkinter.messagebox import *
from math import *


root = Tk()

def cloud(x, y, radius):
    c.create_oval(x + radius * 0.1, y + radius * 0.1, x - radius, y + radius * 0.7, width=0, fill='gray')
    c.create_oval(x - radius * 0.1, y + radius * 0.1, x + radius, y + radius * 0.7, width=0, fill='gray')
    c.create_oval(x + radius * 0.1, y - radius * 0.1, x - radius, y - radius * 0.7, width=0, fill='gray')
    c.create_oval(x - radius * 0.1, y - radius * 0.1, x + radius, y - radius * 0.7, width=0, fill='gray')
    c.create_oval(x - radius * 0.5, y - radius * 0.4, x - radius * 1.5, y + radius * 0.4, width=0, fill='gray')
    c.create_oval(x + radius * 0.5, y - radius * 0.4, x + radius * 1.5, y + radius * 0.4, width=0, fill='gray')
    c.create_oval(x - radius * 0.5, y - radius * 0.5, x + radius * 0.5, y + radius * 0.5, width=0, fill='gray')
def okno(x,y,razm):
    c.create_polygon([x,y],[x + razm,y],[x + razm,y + razm],[x,y + razm],fill = 'gray')
    c.create_line(x,y,x+razm,y,width = 4,fill = 'black')
    c.create_line(x + razm,y,x+razm,y + razm,width = 4,fill = 'black')
    c.create_line(x + razm,y + razm,x,y + razm,width = 4,fill = 'black')
    c.create_line(x,y + razm,x,y,width = 4,fill = 'black')
    c.create_line(x + 1/3*razm,y,x+ 1/3 * razm,y + razm,width = 4,fill = 'black')
    c.create_line(x,y + razm/2,x+1/3*razm,y + razm/2,width = 4,fill = 'black')
def sun(x, y, radius):
    koef = 1.8
    c.create_oval(x - radius/2,y + radius/2,x + radius/2,y - radius/2,fill = 'yellow',width = 0)
    for i in range(0, 180, 20):
        rads = i * 2 * pi / 360
        rx = (radius * koef) * sin(rads)
        ry = (radius * koef) * cos(rads)
        c.create_line(x - rx, y - ry, x + rx, y + ry, width=2, fill='yellow')
def human(x,y):
    c.create_oval(x-5,y-5,x+5,y+5,fill = 'black')
    c.create_line(x,y,x,y+20,fill = 'black')
    c.create_line(x,y+10,x-3,y+15,fill = 'black')
    c.create_line(x,y+10,x+3,y+15,fill = 'black')
    c.create_line(x,y+20,x-3,y+25,fill = 'black')
    c.create_line(x,y+20,x+3,y+25,fill = 'black')
c = Canvas(root,width = 1000,height =  600,bg = 'lightblue',cursor = 'pencil')
#Земля
c.create_line(0,400,1000,400,width = 6, fill = 'gray')
c.create_polygon([0,403],[1000,403],[1000,600],[0,600],fill = 'green')
#Облака
cloud(200,100,50)
cloud(400,130,50)
cloud(700,100,50)
#Дом
c.create_polygon([500,400],[700,400],[700,200],[500,200],fill = 'brown',width = 2,outline = 'black')
#Окна
okno(530,230,50)
okno(630,230,50)
okno(530,330,50)
okno(630,330,50)
#Крыша
c.create_polygon([500,200],[700,200],[600,150],fill = 'brown',width = 2,outline = 'black')
#Труба
c.create_polygon([650,175],[650,150],[660,150],[660,180],fill = 'gray',width = 2,outline = 'black')
#Антена
c.create_line(550,175,550,120,fill = 'black' , width = 2)

c.create_line(550,120,540,130,fill = 'black' , width = 2)
c.create_line(550,120,560,110,fill = 'black' , width = 2)

c.create_line(550,135,560,125,fill = 'black' , width = 2)
c.create_line(550,135,540,145,fill = 'black' , width = 2)

c.create_line(550,150,560,140,fill = 'black',width = 2)
c.create_line(550,150,540,160,fill = 'black',width = 2)

#Солнце
sun(990,10,100)
#Ступеньки и дверь
c.create_polygon([450,400],[500,400],[500,395],[450,395],fill = 'brown')
c.create_polygon([470,395],[500,395],[500,390],[470,390],fill = 'brown')
c.create_polygon([490,390],[500,390],[500,385],[490,385],fill = 'brown')
c.create_polygon([497,385],[500,385],[500,335],[497,335],fill = 'black')
#Текст на крыше
c.create_text(550,180,text="Дом,\n милый дом",
          font="Verdana 12",anchor="w",justify=CENTER,fill="black")
#Табличка
c.create_polygon([840,400],[850,400],[850,350],[840,350],fill = 'gray',width = 2,outline = 'black')
c.create_polygon([800,350],[890,350],[890,300],[800,300],fill = 'gray',width = 4,outline = 'black')
c.create_polygon([890,300],[890,350],[930,325],fill = 'gray',width = 4,outline = 'black')
c.create_text(820,320,text="New York-\n5000km",
          font="Verdana 8",anchor="w",justify=CENTER,fill="black")
#Фонарь
foto = PhotoImage(file = 'fonar2.gif')
c.create_image(190,340,image = foto)
#Человечик
human(280,375)
c.create_text(250,450,text="Панкрахин",
          font="Verdana 12",anchor="w",justify=CENTER,fill="black")
c.pack()
root.mainloop()
