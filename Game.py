from Game_class.Imports import *
pygame.init()
GAME = True
lx = 5000
ly = 5000
# dispInfo = pygame.display.Info()
# SIZE = (dispInfo.current_w,dispInfo.current_h)
SIZE = (1200,600)
posscreen = (-600,SIZE[1] - ly + 600)
window = pygame.display.set_mode(SIZE)
pygame.display.set_caption('СS10')
screen = pygame.Surface((lx,ly))
world = []

for i in range(1):
    A = World(window,screen,posscreen,SIZE,lx,ly)
    world.append(A)
menu = Menu(window,SIZE)

k = 0
command = menu.start()

while GAME:
    if command == 'restart':
        world[k].sprite.winx = posscreen[0]
        world[k].sprite.winy = posscreen[1]
        world[k].sprite.die = False
        command = world[k].start()
    if command == 'resume':
        command = world[k].start()
    if command == 'menu':
        command = menu.start()
    if command == 'lvlup':
        k += 1
        command = world[k].start()
    if command == 'lvldown':
        k -= 1
        command = world[k].start()
    if command == 'close':
        break
pygame.quit()
